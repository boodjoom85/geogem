﻿#include "GeoGrid.h"

#include <iostream>

constexpr int    TOTAL_POINTS_COUNT          = 10000000;
constexpr double FILLED_RECT_POINTS_FRACTION = 0.3;
using namespace geoGem;

int main()
{
  CGeoGrid grid(0.1);

  grid.FillWithRandomPoints(TOTAL_POINTS_COUNT);

  TGeoRect minFilledRect = grid.CalculateMinRectWithPointsCount((int)(TOTAL_POINTS_COUNT * FILLED_RECT_POINTS_FRACTION));
  std::cout << "Minimum Rect with "<< (int) FILLED_RECT_POINTS_FRACTION * 100 <<"% of points: " << minFilledRect << std::endl;

  TGeoRect maxEmptyRect = grid.CalculateMaxEmptyRect();
  std::cout << "Maximum Empty Rect: " << maxEmptyRect << std::endl;

  return 0;
}