#pragma once
#include "GeoGrid.h"
#include <stack>
#include <random>

namespace geoGem
{

const int CGeoGrid::cMaxLatitude        = 90;
const int CGeoGrid::cMaxLongitude       = 180;
const double CGeoGrid::cUnitMeridianLen = 111.0;

CGeoGrid::CGeoGrid(
    double _precision
  ):
  m_Precision(_precision)
{
  m_Grid.resize(cMaxLatitude * 2 * (int) (1 / m_Precision));

  const int maxLatIndex = (int) m_Grid.size();

  for (int latIndex = 0; latIndex < maxLatIndex; ++latIndex)
  {
    m_Grid[latIndex].resize(cMaxLongitude * 2 * (int) (1 / m_Precision));

    const int   maxLonIndex = (int) m_Grid[latIndex].size();
    const double area       = CalculateCellAreaInKm2(latIndex);

    for (int lonIndex = 0; lonIndex < maxLonIndex; ++lonIndex)
      m_Grid[latIndex][lonIndex].Area = area;
  }
}

void CGeoGrid::FillWithRandomPoints(
  int _pointsCount
)
{
  std::random_device rd;

  std::mt19937 latMt(rd());
  std::mt19937 longMt(rd());

  std::uniform_real_distribution<double> latDistribution(-cMaxLatitude, std::nextafter(cMaxLatitude, std::numeric_limits<double>::max()));
  std::uniform_real_distribution<double> longDistribution(-cMaxLongitude, cMaxLongitude);

  for (int i = 0; i < _pointsCount; ++i)
    m_Grid[LatToGridIndex(latDistribution(latMt))][LonToGridIndex(longDistribution(longMt))].GeoPointsCount++;
}

TGeoRect CGeoGrid::CalculateMaxEmptyRect()
{
  const int maxLatIndex = (int)m_Grid.size();
  for (int i = maxLatIndex - 1; i >= 0; --i)
  {
    const int maxLonIndex = (int)m_Grid[i].size();
    for (int j = 0; j < m_Grid[i].size(); ++j)
    {
      if (0 == m_Grid[i][j].GeoPointsCount)
      {
        m_Grid[i][j].UpwardEmptyArea += m_Grid[i][j].Area + (i < m_Grid.size() - 1 ? m_Grid[i + 1][j].UpwardEmptyArea : 0);
        ++m_Grid[i][j].UpwardEmptyCellsCount;
      }
      else
      {
        m_Grid[i][j].UpwardEmptyArea = 0.0;
        m_Grid[i][j].UpwardEmptyCellsCount = 0;
      }
    }
  }

  TGeoRect result;
  result.Area = 0.0;

  for (int i = 0; i < m_Grid.size(); ++i)
  {
    TGeoRect rowMaxSquare = GetMaxAreaInRow(m_Grid[i]);
    if (result.Area < rowMaxSquare.Area)
    {
      result                      = rowMaxSquare;
      result.BottomLeft.Latitude  = GridIndexToLat(i);
      result.BottomRight.Latitude = GridIndexToLat(i);
      result.TopLeft.Latitude     = GridIndexToLat(i + result.HeightInCellsCount);
      result.TopRight.Latitude    = GridIndexToLat(i + result.HeightInCellsCount);
    }
  }

  return result;
}

TGeoRect CGeoGrid::CalculateMinRectWithPointsCount(
  int _pointsCount
)
{
  int southPointsCount = 0;

  int southFirstLonIndex = (int) m_Grid.size();
  int southLastLonIndex  = 0;
  int southLatIndex      = 1;

  for (; southLatIndex < m_Grid.size() && southPointsCount <= _pointsCount; ++southLatIndex)
    for (int lonIndex = 0; lonIndex < m_Grid[southLatIndex].size(); ++lonIndex)
    {
      southPointsCount += m_Grid[southLatIndex][lonIndex].GeoPointsCount;
      if (m_Grid[southLatIndex][lonIndex].GeoPointsCount == 0)
        continue;

      if (southFirstLonIndex > lonIndex)
        southFirstLonIndex = lonIndex;

      if (southLastLonIndex < lonIndex)
        southLastLonIndex = lonIndex;
    }

  double southArea = 0;

  for (int i = 1; i < southLatIndex; ++i)
    southArea += (southLastLonIndex - southFirstLonIndex) * m_Grid[i][0].Area;

  TGeoRect southRect;
  southRect.TopLeft     = TGeoPoint(GridIndexToLat(1), GridIndexToLon(southFirstLonIndex));
  southRect.TopRight    = TGeoPoint(GridIndexToLat(1), GridIndexToLon(southLastLonIndex));
  southRect.BottomLeft  = TGeoPoint(GridIndexToLat(southLatIndex - 1), GridIndexToLon(southFirstLonIndex));
  southRect.BottomRight = TGeoPoint(GridIndexToLat(southLatIndex - 1), GridIndexToLon(southLastLonIndex));
  southRect.Area        = southArea;

  int northPointsCount = 0;

  int northFirstLonIndex = (int) m_Grid.size() + 1;
  int northLastLonIndex  = 0;
  int northLatIndex      = (int) m_Grid.size() - 1;

  for (; northLatIndex >= 0 && northPointsCount <= _pointsCount; --northLatIndex)
    for (int lonIndex = 0; lonIndex < m_Grid[southLatIndex].size(); ++lonIndex)
    {
      northPointsCount += m_Grid[northLatIndex][lonIndex].GeoPointsCount;
      if (m_Grid[northLatIndex][lonIndex].GeoPointsCount == 0)
        continue;

      if (northFirstLonIndex > lonIndex)
        northFirstLonIndex = lonIndex;

      if (northLastLonIndex < lonIndex)
        northLastLonIndex = lonIndex;
    }

  double northArea = 0;

  for (int i = (int) m_Grid.size() - 1; i > northLatIndex; --i)
    northArea += (northLastLonIndex - northFirstLonIndex) * m_Grid[i][0].Area;

  TGeoRect nortRect;
  nortRect.TopLeft     = TGeoPoint(GridIndexToLat((int) m_Grid.size() - 1), GridIndexToLon(northFirstLonIndex));
  nortRect.TopRight    = TGeoPoint(GridIndexToLat((int) m_Grid.size() - 1), GridIndexToLon(northLastLonIndex));
  nortRect.BottomLeft  = TGeoPoint(GridIndexToLat(northLatIndex + 1), GridIndexToLon(northFirstLonIndex));
  nortRect.BottomRight = TGeoPoint(GridIndexToLat(northLatIndex + 1), GridIndexToLon(northLastLonIndex));
  nortRect.Area        = northArea;

  return northArea > southArea ? nortRect : southRect;
}

int CGeoGrid::LatToGridIndex(
    double _lat
  )
{
  const double precisionCoeff = 1 / m_Precision;
  int result = cMaxLatitude * (int) precisionCoeff + (int) floor(_lat * precisionCoeff);

  if (result >= cMaxLatitude * 2 * (int) precisionCoeff)
    return cMaxLatitude * 2 * (int) precisionCoeff - 1;

  return result;
}

int CGeoGrid::LonToGridIndex(
    double _lon
  )
{
  const double precisionCoeff = 1 / m_Precision;
  return cMaxLongitude * (int) precisionCoeff + (int) floor(_lon * precisionCoeff);
}


double CGeoGrid::GridIndexToLat(
    int _index
  )
{
  const double precisionCoeff = 1 / m_Precision;
  return (_index - cMaxLatitude * precisionCoeff) / precisionCoeff;
}

double CGeoGrid::GridIndexToLon(
    int _index
  )
{
  const double precisionCoeff = 1 / m_Precision;
  return (_index - cMaxLongitude * precisionCoeff) / precisionCoeff;
}

double CGeoGrid::CalculateCellAreaInKm2(
    int _indexLat
  )
{
  double width  = Distance(TGeoPoint(GridIndexToLat(_indexLat), 0.0), TGeoPoint(GridIndexToLat(_indexLat), 0.1));
  double height = cUnitMeridianLen * m_Precision; // length of degree latitude, don't depends from longitude
  return width * height;
}

TGeoRect CGeoGrid::GetMaxAreaInRow(
    const std::vector<TGeoGridCell> & _row
  )
{
  int cellsCount = (int)_row.size();
  if (0 == cellsCount)
    return TGeoRect();

  std::stack<int> left;
  std::stack<int> right;

  struct TInterval {
    int LeftCount;
    int RightCount;
  };

  std::vector<TInterval> width(cellsCount);

  for (int i = 0; i < cellsCount; ++i)
  {
    while (!left.empty() && _row[i].UpwardEmptyArea <= _row[left.top()].UpwardEmptyArea)
      left.pop();

    left.push(i);
  }

  for (int i = 0; i < cellsCount; i++)
  {
    while (!left.empty() && _row[i].UpwardEmptyArea <= _row[left.top()].UpwardEmptyArea)
      left.pop();

    if (left.empty())
      width[i].LeftCount += i;
    else
      width[i].LeftCount += i - 1 - (i <= left.top() ? left.top() - cellsCount : left.top());

    left.push(i);
  }

  for (int i = cellsCount - 1; i >= 0; i--)
  {
    while (!right.empty() && _row[i].UpwardEmptyArea <= _row[right.top()].UpwardEmptyArea)
      right.pop();

    right.push(i);
  }

  for (int i = cellsCount - 1; i >= 0; i--)
  {
    while (!right.empty() && _row[i].UpwardEmptyArea <= _row[right.top()].UpwardEmptyArea)
      right.pop();

    if (right.empty())
      width[i].RightCount += cellsCount - 1 - i;
    else
      width[i].RightCount += (i >= right.top() ? right.top() + cellsCount : right.top()) - i - 1;

    right.push(i);
  }

  TGeoRect maxRect;

  for (int i = 0; i < cellsCount; i++)
    if (maxRect.Area < (width[i].LeftCount + width[i].RightCount + 1) * _row[i].UpwardEmptyArea)
    {
      maxRect.BottomLeft         = TGeoPoint(0.0, GridIndexToLon(width[i].LeftCount > i ? cellsCount + i - width[i].LeftCount : i - width[i].LeftCount));
      maxRect.TopLeft            = TGeoPoint(0.0, GridIndexToLon(width[i].LeftCount > i ? cellsCount + i - width[i].LeftCount : i - width[i].LeftCount));
      maxRect.BottomRight        = TGeoPoint(0.0, GridIndexToLon(width[i].RightCount + i >= cellsCount ? i + 1 + width[i].RightCount - cellsCount : i + 1 + width[i].RightCount));
      maxRect.TopRight           = TGeoPoint(0.0, GridIndexToLon(width[i].RightCount + i >= cellsCount ? i + 1 + width[i].RightCount - cellsCount : i + 1 + width[i].RightCount));
      maxRect.Area               = (width[i].LeftCount + width[i].RightCount + 1) * _row[i].UpwardEmptyArea;
      maxRect.HeightInCellsCount = _row[i].UpwardEmptyCellsCount;
    }

  return maxRect;
}

std::ostream& operator<<(
    std::ostream&   _os,
    const TGeoRect& _rect
  )
{
  _os << "GeoRectangle:\n";
  _os << "TopLeft: "     << _rect.TopLeft;
  _os << "TopRight: "    << _rect.TopRight;
  _os << "BottomLeft: "  << _rect.BottomLeft;
  _os << "BottomRight: " << _rect.BottomRight;
  _os << "Area: "        << _rect.Area << "\n";
  return _os;
}

}// namespace GeoGem