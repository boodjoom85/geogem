#pragma once
#include <iostream>

namespace geoGem
{

constexpr double M_PI = 3.14159265358979323846;

constexpr double DegreesToRadians(
    const double _degree
  )
{
  return (M_PI / 180 * _degree);
}

struct TGeoPoint
{
  double Latitude  = 0.0f;
  double Longitude = 0.0f;

  TGeoPoint() = default;
  explicit TGeoPoint(double _lat, double _lon) : Latitude(_lat), Longitude(_lon) {};
};

inline std::ostream& operator<<(
    std::ostream&    _os,
    const TGeoPoint& _point
  )
{
  _os << _point.Latitude << " " << _point.Longitude << "\n";
  return _os;
}

inline double Distance(TGeoPoint _point1, TGeoPoint _point2)
{
  double latStart = DegreesToRadians(_point1.Latitude);
  double lonStart = DegreesToRadians(_point1.Longitude);
  double latEnd   = DegreesToRadians(_point2.Latitude);
  double lonEnd   = DegreesToRadians(_point2.Longitude);

  // Use Haversine Formula 
  double deltaLat  = latStart - latEnd;
  double deltaLong = lonStart - lonEnd;

  double distance = pow(sin(deltaLat / 2.0), 2.0) +  cos(latStart) * cos(latEnd) * pow(sin(deltaLong / 2.0), 2.0);
  distance = 2 * atan2(sqrt(distance), sqrt(1.0 - distance));

  const double earthRadiusInKm = 6378.137;

  return distance * earthRadiusInKm;
}

}// namespace GeoGem