#pragma once
#include "GeoUtils.h"
#include <iostream>
#include <vector>

namespace geoGem
{

struct TGeoGridCell
{
  int    GeoPointsCount        = 0;
  double Area                  = 0.0f;
  double UpwardEmptyArea       = 0.0f;
  int    UpwardEmptyCellsCount = 0;
};

struct TGeoRect
{
  TGeoPoint TopLeft            = TGeoPoint(0, 0);
  TGeoPoint TopRight           = TGeoPoint(0, 0);
  TGeoPoint BottomLeft         = TGeoPoint(0, 0);
  TGeoPoint BottomRight        = TGeoPoint(0, 0);
  double    Area               = 0.0f;
  int       HeightInCellsCount = -1;
  int       WidthInCellsCount  = -1;
};

class CGeoGrid
{

public: //Construction

  CGeoGrid(double _precision = 1.0f);

public: // interface
  
  void FillWithRandomPoints( 
      int _pointsCount
    );

  TGeoRect CalculateMaxEmptyRect();
  TGeoRect CalculateMinRectWithPointsCount(
      int _pointsCount
    );

protected: // service

  int LatToGridIndex(
      double _lat
    );

  int LonToGridIndex(
      double _lon
    );
   
  double GridIndexToLat(
      int _index
    );
     
  double GridIndexToLon(
      int _index
    );

  double CalculateCellAreaInKm2(
      int _indexLat
    );

  TGeoRect GetMaxAreaInRow(
      const std::vector<TGeoGridCell> & row
    );

private: // Members

  std::vector<std::vector<TGeoGridCell>> m_Grid;
  int                                    m_MaxLatIndex;
  int                                    m_MaxLonIndex;
  double                                 m_Precision;

private: // Consts
  static const int    cMaxLatitude;
  static const int    cMaxLongitude;
  static const double cUnitMeridianLen;
};

std::ostream& operator<<(
    std::ostream&   _os,
    const TGeoRect& _rect
  );

}// namespace GeoGem